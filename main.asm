%include "lib.inc"
%include "dict.inc"
%include "words.inc"

section .bss
buffer: resb 256	; буфер для хранения прочитанного слова

section .rodata
TOO_LONG: db "String is too long, it must be less than 256 symbols", 0	; строка, выводимая в stderr, если слово слишком длинное
NOT_FOUND: db "String not found in the dictionary", 0	; строка, выводимая в stderr, если слово не найдено в словаре

section .text
global _start

_start:
    mov rdi, buffer		; сохраняем в rdi указатель на буфер
    mov rsi, 256		; сохраняем в rsi размер буфера
    push rdi			; сохраняем значение rdi в стек
    call read_word		; вызываем функцию, сохраняющую в rax адрес прочитанного в буфер слова или 0, если слово не было прочитано
    pop rdi			; возвращаем изначальное значение rdi
    test rax, rax		; проверка rax на 0
    jz .too_long		; если rax = 0, то слово было слишком длинным,чтобы его прочесть

    mov rsi, indicator		; сохраняем в rsi указатель на начало словаря (созданного при помощи colon)
    call find_word		; вызываем функцию, сохраняющую в rax 0, если слово не найдено в словаре
                                ; а если слово найдено, то сохраняет адрес начала вхождения в словарь элемента с совпавшим ключом
    test rax, rax		; проверка rax на 0
    jz .not_found		; если rax = 0, то слово не было найдено в словаре

    mov rdi, rax		; сохраняем в rdi адрес начала вхождения в словарь подходящего элемента
    add rdi, 8			; получаем в rdi адрес ключа этого элемента
    push rdi			; сохраняем значение rdi в стек
    call string_length		; вызываем функцию, сохраняющую в rax длину ключа
    pop rdi			; возвращаем изначальное значение rdi
    add rdi, rax		; получаем в rdi адрес нуль-терминатора ключа
    inc rdi 			; получаем в rdi адрес значения
    call print_string		; вызываем функцию, выводящую полученное значение в stdout
    call print_newline		; перенос строки
    call exit			; завершение текущего процесса
.too_long:
    mov rdi, TOO_LONG		; сохраняем в rdi указатель на строку TOO_LONG
    jmp .error			; переходим к выводу ошибки в stderr
.not_found:
    mov rdi, NOT_FOUND		; сохраняем в rdi указатель на строку NOT_FOUND
.error:
    call print_error_string	; вызываем функцию, выводящую полученную ошибку в stderr
    call print_newline		; перенос строки
    call exit			; завершение текущего процесса
