import subprocess

inputs = ["name", "surname", "patronymic", "not_found", "toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo_long"]
outputs = ["Kirill", "Krakov", "Konstantinovich", "", ""]
errors = ["", "", "", "String not found in the dictionary", "String is too long, it must be less than 256 symbols"]

for i in range(len(inputs)):
    process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=inputs[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()
    print("TEST #" + str(i+1) + ": " + inputs[i])
    if stdout == outputs[i] and stderr == errors[i]:
        print("RESULT OF TEST #" + str(i+1) + ": CORRECT")
    else:
        print("RESULT OF TEST #" + str(i+1) + ": INCORRECT")

        if stdout != outputs[i]:
            print("Expected in STDOUT: " + outputs[i])
            print("Actually in STDOUT: " + stdout)
        if stderr != errors[i]:
            print("Expected in STDERR: " + errors[i])
            print("Actually in STDERR: " + stderr)
    print("---------")
